from django.apps import AppConfig


class StatustiketConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'statusTiket'
