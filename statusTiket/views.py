from django.shortcuts import redirect, render
from django.db import connection
from .models import *   

# Create your views here.

def StatusTiket(request):
    response = {}
    response['status_tiket'] = []

    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("""
            SELECT kode, nama_status 
            FROM STATUS_TIKET
            """)
        status = cursor.fetchall()

        for i in range(len(status)):
            response['status_tiket'].append(
                status[i]
            )
        cursor.execute("set search_path to public")
    return render(request, 'status_tiket.html', response)

def editStatus(request, kode):
    context = {
        'kode' : kode
    } 
    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                UPDATE STATUS_TIKET SET
                nama_status = '{request.POST['nama_status_baru']}'
                WHERE kode = '{kode}';
                """)
            cursor.execute("set search_path to public")
            return redirect('/status-tiket')
    return render(request, 'update_status.html', context)

def createStatus(request):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""
            SELECT kode FROM STATUS_TIKET
            ORDER BY kode desc
            limit 1;
            """)
        last_dist = cursor.fetchall()
        new_kode = int(last_dist[0][0])+1
        if len(str(new_kode)) == 1:
            new_kode = f"0{new_kode}"
        else:
            new_kode = f"{new_kode}"

        context = {
            'auto_kode' : new_kode
        }

        cursor.execute("set search_path to public")

    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                INSERT INTO STATUS_TIKET VALUES (
                    '{new_kode}',
                    '{request.POST['nama_status']}'
                );
                """)
            cursor.execute("set search_path to public")
            return redirect('/status-tiket')

    return render(request, 'create_status.html', context)

def deleteStatus(request, kode):
    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                UPDATE TIKET SET
                kode_status = null
                WHERE kode_status = '{kode}';
                """)
            cursor.execute(f"""
                DELETE FROM STATUS_TIKET
                WHERE kode = '{kode}';
                """)
            
            cursor.execute("set search_path to public")
            return redirect("/status-tiket")
    return render(request, 'delete_status.html')