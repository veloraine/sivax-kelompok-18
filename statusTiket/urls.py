from django.urls import path
from . import views

urlpatterns = [
    path('', views.StatusTiket),
    path('update-status/(?P<kode>[0-9]+)$', views.editStatus, name="update_status"),
    path('create-status', views.createStatus, name="create_status"),
    path('delete-status/(?P<kode>[0-9]+)$', views.deleteStatus, name="delete_status"),
]