from django.urls import path
from . import views

urlpatterns = [
    path('tambah/', views.tambah),
    path('update-pengajuan/(?P<nama_instansi>[0-9]+)$/(?P<status>[0-9]+)$', views.update_pengajuan, name='update_jadwal'),
    path('detail-penjadwalan/(?P<nama_instansi>[0-9]+)$/(?P<tanggal_waktu>[0-9]+)$/(?P<jumlah>[0-9]+)$/(?P<status>[0-9]+)$/(?P<kode_distribusi>[0-9]+)$', views.detail_penjadwalan, name='detail_jadwal'),
    path('detail-pengajuan/(?P<nama_instansi>[0-9]+)$/(?P<tanggal_waktu>[0-9]+)$/(?P<jumlah>[0-9]+)$/(?P<status>[0-9]+)$/(?P<kode_distribusi>[0-9]+)$', views.detail_pengajuan, name='detail_pengajuan'),
    path('pengajuan/', views.pengajuan),
    path('penjadwalan/', views.penjadwalan),
    path('verifikasi/(?P<nama_instansi>[0-9]+)$/(?P<status>[0-9]+)$', views.verifikasi, name='verifikasi')
]