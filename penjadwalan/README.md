# Module Penjadwalan (Fitur 3)

**Dibuat oleh Brandon Ivander - 2006535874**

## URLs

| namespace / use case  | URL 					 | 
| --------------------- | ---------------------- | 
| tambah                | /penjadwalan/tambah/                |
| update_pengajuan          | /penjadwalan/update-pengajuan/(?P<nama_instansi>[0-9]+)$/(?P<status>[0-9]+)$|
| detail_penjadwalan | /penjadwalan/detail-penjadwalan/(?P<nama_instansi>[0-9]+)$/(?P<tanggal_waktu>[0-9]+)$/(?P<jumlah>[0-9]+)$/(?P<status>[0-9]+)$/(?P<kode_distribusi>[0-9]+)$ | 
| detail_pengajuan | /penjadwalan/detail-pengajuan/(?P<nama_instansi>[0-9]+)$/(?P<tanggal_waktu>[0-9]+)$/(?P<jumlah>[0-9]+)$/(?P<status>[0-9]+)$/(?P<kode_distribusi>[0-9]+)$ |
| pengajuan | /penjadwalan/pengajuan/ |
| penjadwalan | /penjadwalan/penjadwalan// |
| verifikasi | /penjadwalan/verifikasi/(?P<nama_instansi>[0-9]+)$(?P<status>[0-9]+)$ |