from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages

# Create your views here.
def tambah(request):
    response = {}
    response['instansi'] = []
    response['lokasi_vaksin'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * from INSTANSI")
        instansi = cursor.fetchall()
        
        for i in range(len(instansi)):
            response['instansi'].append([
                instansi[i][0], instansi[i][1]
            ])

        cursor.execute("SELECT kode, nama from LOKASI_VAKSIN")
        lokasi_vaksin = cursor.fetchall()
        
        for i in (lokasi_vaksin):
            response['lokasi_vaksin'].append(i)

        cursor.execute("set search_path to public")

    if request.method == "POST":
        # tinggal tambahin insert ke database, tapi harus bisa nambah instansi dulu?
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            try:
                cursor.execute(f"""INSERT INTO PENJADWALAN VALUES (
                    '{request.POST['kode_instansi']}', 
                    '{request.POST['tanggal_waktu']}',
                    '{request.POST['jumlah']}',
                    '{request.POST['kategori_penerima']}',
                    'pengajuan dikirim',
                    0,
                    '{request.POST['kode_lokasi']}',
                    '',
                    NULL
                    );""")
                print('SUKSES MEMBUAT PENJADWALAN')
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

            cursor.execute("set search_path to public")
            return redirect("/penjadwalan/penjadwalan")  
    return render(request, 'tambah.html', response)

def detail_penjadwalan(request, nama_instansi, tanggal_waktu, jumlah, status, kode_distribusi):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"SELECT kode FROM INSTANSI WHERE nama_instansi = '{nama_instansi}'")
        kode_instansi = cursor.fetchall()

        cursor.execute(f"""
            SELECT kategori_penerima, jumlah_nakes, email_admin, kode_lokasi 
            from PENJADWALAN
            WHERE kode_instansi = '{kode_instansi[0][0]}';
            """)
        data = cursor.fetchall()

        cursor.execute(f"""
            SELECT nama
            from LOKASI_VAKSIN
            WHERE kode = '{data[0][3]}'
            """)
        nama_lokasi = cursor.fetchall()

        # jika ada kode distribusi
        tanggal_dist = ''
        biaya_dist = ''
        jumlah_vaksin = ''
        nama_vaksin = ''
        if (kode_distribusi != 'None'):
            cursor.execute(f"""
                SELECT tanggal, biaya, jumlah_vaksin, nama
                from DISTRIBUSI, VAKSIN
                WHERE DISTRIBUSI.kode_vaksin = VAKSIN.kode
                AND DISTRIBUSI.kode = '{kode_distribusi}';
                """)
            data_distribusi = cursor.fetchall()
            tanggal_dist = data_distribusi[0][0]
            biaya_dist = data_distribusi[0][1]
            jumlah_vaksin = data_distribusi[0][2]
            nama_vaksin = data_distribusi[0][3]
            
        context = {
            'nama_instansi' : nama_instansi,
            'tanggal_waktu' : tanggal_waktu,
            'jumlah' : jumlah,
            'status' : status,
            'kode_distribusi' : kode_distribusi,
            'kategori_penerima' : data[0][0],
            'jumlah_nakes' : data[0][1],
            'email_admin' : data[0][2],
            'nama_lokasi' : nama_lokasi[0][0],
            'tanggal_dist' : tanggal_dist,
            'biaya_dist' : biaya_dist,
            'jumlah_vaksin' : jumlah_vaksin,
            'nama_vaksin' : nama_vaksin
        }

        cursor.execute("set search_path to public")
    return render(request, 'detail_penjadwalan.html', context)

def detail_pengajuan(request, nama_instansi, tanggal_waktu, jumlah, status, kode_distribusi):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"SELECT kode FROM INSTANSI WHERE nama_instansi = '{nama_instansi}'")
        kode_instansi = cursor.fetchall()

        cursor.execute(f"""
            SELECT kategori_penerima, jumlah_nakes, email_admin, kode_lokasi 
            from PENJADWALAN
            WHERE kode_instansi = '{kode_instansi[0][0]}';
            """)
        data = cursor.fetchall()

        cursor.execute(f"""
            SELECT nama
            from LOKASI_VAKSIN
            WHERE kode = '{data[0][3]}'
            """)
        nama_lokasi = cursor.fetchall()

        # jika ada kode distribusi
        tanggal_dist = ''
        biaya_dist = ''
        jumlah_vaksin = ''
        nama_vaksin = ''
        if (kode_distribusi != 'None'):
            cursor.execute(f"""
                SELECT tanggal, biaya, jumlah_vaksin, nama
                from DISTRIBUSI, VAKSIN
                WHERE DISTRIBUSI.kode_vaksin = VAKSIN.kode
                AND DISTRIBUSI.kode = '{kode_distribusi}';
                """)
            data_distribusi = cursor.fetchall()
            tanggal_dist = data_distribusi[0][0]
            biaya_dist = data_distribusi[0][1]
            jumlah_vaksin = data_distribusi[0][2]
            nama_vaksin = data_distribusi[0][3]
            
        context = {
            'nama_instansi' : nama_instansi,
            'tanggal_waktu' : tanggal_waktu,
            'jumlah' : jumlah,
            'status' : status,
            'kode_distribusi' : kode_distribusi,
            'kategori_penerima' : data[0][0],
            'jumlah_nakes' : data[0][1],
            'email_admin' : data[0][2],
            'nama_lokasi' : nama_lokasi[0][0],
            'tanggal_dist' : tanggal_dist,
            'biaya_dist' : biaya_dist,
            'jumlah_vaksin' : jumlah_vaksin,
            'nama_vaksin' : nama_vaksin
        }
        cursor.execute("set search_path to public")
    return render(request, 'detail_pengajuan.html', context)

def penjadwalan(request):
    response = {}
    response['penjadwalan'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute('''
            SELECT nama_instansi, tanggal_waktu, jumlah, status, kode_distribusi
            FROM PENJADWALAN, INSTANSI
            WHERE PENJADWALAN.kode_instansi = INSTANSI.kode
            ''')
        penjadwalan = cursor.fetchall()

        for i in range(len(penjadwalan)):
            response['penjadwalan'].append(
                penjadwalan[i]
            )
        cursor.execute("set search_path to public")
    return render(request, 'daftar_penjadwalan.html', response)

def pengajuan(request):
    response = {}
    response['penjadwalan'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute('''
            SELECT nama_instansi, tanggal_waktu, jumlah, status, kode_distribusi
            FROM PENJADWALAN, INSTANSI
            WHERE PENJADWALAN.kode_instansi = INSTANSI.kode
            ''')
        penjadwalan = cursor.fetchall()

        for i in range(len(penjadwalan)):
            response['penjadwalan'].append(
                penjadwalan[i]
            )
        cursor.execute("set search_path to public")
    return render(request, 'daftar_pengajuan.html', response)

def update_pengajuan(request, nama_instansi, status):
    response = {}
    response['instansi'] = []
    response['lokasi_vaksin'] = []
    response['status'] = status
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * from INSTANSI")
        instansi = cursor.fetchall()
        
        for i in range(len(instansi)):
            response['instansi'].append([
                instansi[i][0], instansi[i][1]
            ])

        cursor.execute("SELECT kode, nama from LOKASI_VAKSIN")
        lokasi_vaksin = cursor.fetchall()
        
        for i in (lokasi_vaksin):
            response['lokasi_vaksin'].append(i)

        cursor.execute("set search_path to public")

    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"SELECT kode FROM INSTANSI WHERE nama_instansi = '{nama_instansi}'")
            kode_lama = cursor.fetchall()
            # try:
            cursor.execute(f"""
                UPDATE PENJADWALAN SET
                kode_instansi = '{request.POST['kode_instansi']}',
                tanggal_waktu = '{request.POST['tanggal_waktu']}',
                jumlah = '{request.POST['jumlah']}',
                kategori_penerima = '{request.POST['kategori_penerima']}',
                kode_lokasi = '{request.POST['kode_lokasi']}',
                status = 'pengajuan dikirim'
                WHERE kode_instansi = '{kode_lama[0][0]}';
                """)
            print('SUKSES UPDATE PENJADWALAN')
            # except:
            #     messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
            cursor.execute("set search_path to public")
            return redirect("/penjadwalan/penjadwalan") 
    return render(request, 'update_pengajuan.html', response)

def verifikasi(request, nama_instansi, status):
    # TODO: get session mail
    email_verif = request.session['email']
    response = {}
    response['instansi'] = []
    response['lokasi_vaksin'] = []
    response['faskes'] = False
    response['status'] = status
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * from INSTANSI")
        instansi = cursor.fetchall()
        
        for i in range(len(instansi)):
            response['instansi'].append([
                instansi[i][0], instansi[i][1]
            ])

        cursor.execute("SELECT kode, nama from LOKASI_VAKSIN")
        lokasi_vaksin = cursor.fetchall()
        
        for i in (lokasi_vaksin):
            response['lokasi_vaksin'].append(i)

        # cek 
        
        cursor.execute(f"SELECT kode FROM INSTANSI WHERE nama_instansi = '{nama_instansi}'")
        kode_one = cursor.fetchall()

        cursor.execute(f"""
            SELECT kode_instansi 
            from INSTANSI_FASKES
            WHERE kode_instansi = '{kode_one[0][0]}';
            """)
        kode_two = cursor.fetchall()
        try:
            if (kode_one[0][0] == kode_two[0][0]):
                response['faskes'] = True
        except:
            pass
        cursor.execute("set search_path to public")

    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"SELECT kode FROM INSTANSI WHERE nama_instansi = '{nama_instansi}'")
            kode_lama = cursor.fetchall()
            try:    
                cursor.execute(f"""
                    UPDATE PENJADWALAN SET
                    kode_instansi = '{request.POST['kode_instansi']}',
                    tanggal_waktu = '{request.POST['tanggal_waktu']}',
                    jumlah = '{request.POST['jumlah']}',
                    kategori_penerima = '{request.POST['kategori_penerima']}',
                    kode_lokasi = '{request.POST['kode_lokasi']}',
                    status = '{request.POST['submit']}',
                    email_admin = '{email_verif}'
                    WHERE kode_instansi = '{kode_lama[0][0]}';
                    """)
            except:
                print('Tidak sukses')
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
            cursor.execute("set search_path to public")
            return redirect("/penjadwalan/pengajuan") 
    return render(request, 'verifikasi.html', response)