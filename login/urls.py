from django.urls import path
from . import views

urlpatterns = [
    path('register', views.register, name='register'),
    path('register_warga', views.register_warga, name='register_warga'),
    path('register_admin', views.register_admin_sistem, name='register_admin'),
    path('register_panitia', views.register_panitia_penyelenggara, name='register_panitia'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name="logout"),
    path('register_vaksinasi', views.register_vaksinasi, name='register_vaksinasi'),
]