# Cek apakah password valid
def validate_password(password):
    flag_capital = False
    flag_num = False

    # Cek apakah ada yang kapital dan digit
    for char in password:
        if char.isupper():
            flag_capital = True
        elif char.isdigit():
            flag_num = True
    
    return (flag_capital and flag_num)
