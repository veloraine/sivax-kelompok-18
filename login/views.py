from django.shortcuts import render, redirect
from .models import *
from django.db.utils import InternalError
from django.contrib import messages
from .validators import validate_password
from django.db import connection

def register(request):
    # Check apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("home")
    
    return render(request, 'register.html')

def register_warga(request):
    # Check apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("home")

    # Data dummy instansi
    response = {}
    response['instansi'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * from INSTANSI")
        instansi = cursor.fetchall()
        
        for i in range(len(instansi)):
            response['instansi'].append([
                instansi[i][0], instansi[i][1]
            ])
        cursor.execute("set search_path to public")
            
    if request.method == "POST":
        password_validity = validate_password(request.POST["password"])
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                    SELECT FROM PENGGUNA WHERE email = '{request.POST["email"]}'
                """)
            pengguna = cursor.fetchall()
        
        # Cek password valid dan pengguna belum terdaftar
        if not password_validity and len(pengguna) == 0:    
            messages.add_message(request, messages.WARNING, f"Password harus setidaknya 6 huruf dengan 1 huruf kapital dan 1 angka")
        else: # Kalau valid semua, register
            with connection.cursor() as cursor:
                cursor.execute("set search_path to sivax")
                try:
                    
                    cursor.execute(f"""
                        SELECT * FROM PENGGUNA WHERE email = '{request.POST["email"]}'
                    """)
                    
                    pengguna = cursor.fetchall()
                    # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                    if (len(pengguna) == 0):
                        cursor.execute(f"""
                            INSERT INTO PENGGUNA (email, no_telp, password, status_verifikasi) VALUES
                            ('{request.POST["email"]}', '{request.POST["no_telp"]}', '{request.POST["password"]}', 'belum terverifikasi')
                        """)
                    # Cek apakah no_telp dan password sesuai dengan register pertama
                    elif (request.POST["no_telp"] != pengguna[0][1] or request.POST["password"] != pengguna[0][2]):
                            messages.add_message(request, messages.WARNING, f"Harap masukkan nomor telepon dan password yang sesuai dengan register sebelumnya")
                            cursor.execute("set search_path to public")
                            return render(request, "register_warga.html", response)
                    elif (request.POST["no_telp"] != pengguna[0][1]):
                            messages.add_message(request, messages.WARNING, f"Harap masukkan nomor telepon yang sesuai dengan register sebelumnya")
                            cursor.execute("set search_path to public")
                            return render(request, "register_warga.html", response)
                    elif (request.POST["password"] != pengguna[0][2]):
                            messages.add_message(request, messages.WARNING, f"Harap masukkan password yang sesuai dengan register sebelumnya")
                            cursor.execute("set search_path to public")
                            return render(request, "register_warga.html", response)
                                                
                    cursor.execute(f"""
                        INSERT INTO WARGA (email, nik, nama_lengkap, jenis_kelamin, no, jalan, kelurahan, kecamatan, kabkot, instansi) VALUES
                        ('{request.POST["email"]}', '{request.POST["nik"]}', '{request.POST["nama_lengkap"]}', '{request.POST["jenis_kelamin"]}',
                        '{request.POST["no"]}', '{request.POST["jalan"]}', '{request.POST["kelurahan"]}', '{request.POST["kecamatan"]}', 
                        '{request.POST["kabkot"]}', '{request.POST["instansi"]}')
                    """)
                    
                    messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")
                    cursor.execute("set search_path to public")

                    return redirect("login")
                
                # Ada email yang sama, handle disini
                except InternalError:
                    messages.add_message(request, messages.WARNING, f"{request.POST['email']} sudah terdaftar sebagai Warga")
                except:
                    messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
                    
        cursor.execute("set search_path to public")
    return render(request, "register_warga.html", response)

def register_panitia_penyelenggara(request):
    # Cek apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("home")

    response = {}
    response['instansi'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * from INSTANSI")
        instansi = cursor.fetchall()
        
        for i in range(len(instansi)):
            response['instansi'].append([
                instansi[i][0], instansi[i][1]
            ])
        cursor.execute("set search_path to public")
            
    if request.method == "POST":
        password_validity = validate_password(request.POST["password"])
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                    SELECT * FROM PENGGUNA WHERE email = '{request.POST["email"]}'
                """)
            pengguna = cursor.fetchall()
        
        # Password & Username valid?
        if not password_validity and len(pengguna) == 0:            
            messages.add_message(request, messages.WARNING, f"Password harus setidaknya 6 huruf dengan 1 huruf kapital dan 1 angka")
        else: # Kalau valid semua, register
            with connection.cursor() as cursor:
                try:
                    # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                    if (len(pengguna) == 0):
                        cursor.execute(f"""
                            INSERT INTO PENGGUNA (email, no_telp, password, status_verifikasi) VALUES
                            ('{request.POST["email"]}', '{request.POST["no_telp"]}', '{request.POST["password"]}', 'belum terverifikasi')
                        """)  
                        
                    # Cek apakah no_telp dan password sesuai dengan register pertama
                    elif (request.POST["no_telp"] != pengguna[0][1] or request.POST["password"] != pengguna[0][2]):
                        messages.add_message(request, messages.WARNING, f"Harap masukkan nomor telepon dan password yang sesuai dengan register sebelumnya")
                        cursor.execute("set search_path to public")
                        return render(request, "register_panitia.html", response)
                    elif (request.POST["no_telp"] != pengguna[0][1]):
                        messages.add_message(request, messages.WARNING, f"Harap masukkan nomor telepon yang sesuai dengan register sebelumnya")
                        cursor.execute("set search_path to public")
                        return render(request, "register_panitia.html", response)
                    elif (request.POST["password"] != pengguna[0][2]):
                        messages.add_message(request, messages.WARNING, f"Harap masukkan password yang sesuai dengan register sebelumnya")
                        cursor.execute("set search_path to public")
                        return render(request, "register_panitia.html", response)
                        
                    cursor.execute(f"""
                        SELECT FROM ADMIN_SATGAS WHERE email = '{request.POST["email"]}'
                    """)

                    # Kalau gak ada ADMIN_SISTEM dengan email sama maka lanjut bikinin
                    if (len(cursor.fetchall()) == 0):
                        cursor.execute(f"""
                            INSERT INTO PANITIA_PENYELENGGARA (email, nama_lengkap) VALUES
                            ('{request.POST["email"]}', '{request.POST["nama_lengkap"]}')
                        """)
                        
                        if request.POST["tenaga_kesehatan"] == "Ya":                    
                            cursor.execute(f"""
                                INSERT INTO NAKES (email, no_str, tipe) VALUES
                                ('{request.POST["email"]}', '{request.POST["no_str"]}', '{request.POST["tipe"]}')
                            """)
                            
                        messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")
                        return redirect("login")
                            
                    else:
                        messages.add_message(request, messages.WARNING, f"{request.POST['email']} sudah terdaftar sebagai ADMIN_SATGAS")

                except:
                    messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
                    
                cursor.execute("set search_path to public")
    return render(request, "register_panitia.html", response)

def register_admin_sistem(request):
    # Cek apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("home")

    response = {}
    response['instansi'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * from INSTANSI")
        instansi = cursor.fetchall()
        
        for i in range(len(instansi)):
            response['instansi'].append([
                instansi[i][0], instansi[i][1]
            ])
        cursor.execute("set search_path to public")

    if request.method == "POST":
        password_validity = validate_password(request.POST["password"])
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                    SELECT * FROM PENGGUNA WHERE email = '{request.POST["email"]}'
                """)
            pengguna = cursor.fetchall()

        # Password & Username valid?
        if not password_validity and len(pengguna) == 0:
            messages.add_message(request, messages.WARNING, f"Password harus setidaknya 6 huruf dengan 1 huruf kapital dan 1 angka")
        else: # Kalau valid semua, register
            with connection.cursor() as cursor:
                try:
                    # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                    if (len(pengguna) == 0):
                        cursor.execute(f"""
                            INSERT INTO PENGGUNA (email, no_telp, password, status_verifikasi) VALUES
                            ('{request.POST["email"]}', '{request.POST["no_telp"]}', '{request.POST["password"]}', 'belum terverifikasi')
                        """)

                    # Cek apakah no_telp dan password sesuai dengan register pertama
                    elif (request.POST["no_telp"] != pengguna[0][1] and request.POST["password"] != pengguna[0][2]):
                            messages.add_message(request, messages.WARNING, f"Harap masukkan nomor telepon dan password yang sesuai dengan register sebelumnya")
                            cursor.execute("set search_path to public")
                            return render(request, "register_admin.html", response)
                    elif (request.POST["no_telp"] != pengguna[0][1]):
                            messages.add_message(request, messages.WARNING, f"Harap masukkan nomor telepon yang sesuai dengan register sebelumnya")
                            cursor.execute("set search_path to public")
                            return render(request, "register_admin.html", response)
                    elif (request.POST["password"] != pengguna[0][2]):
                            messages.add_message(request, messages.WARNING, f"Harap masukkan password yang sesuai dengan register sebelumnya")
                            cursor.execute("set search_path to public")
                            return render(request, "register_admin.html", response)
                    
                    cursor.execute(f"""
                        SELECT FROM PANITIA_PENYELENGGARA WHERE email = '{request.POST["email"]}'
                    """)

                    # Kalau gak ada PANITIA_PENYELENGGARA dengan email sama maka lanjut bikinin
                    if (len(cursor.fetchall()) == 0):
                        cursor.execute(f"""
                            INSERT INTO ADMIN_SATGAS (email, id_pegawai) VALUES
                            ('{request.POST["email"]}', '{request.POST["id_pegawai"]}')
                        """)
                        
                        messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")

                        return redirect("login")
                
                    else:
                        messages.add_message(request, messages.WARNING, f"{request.POST['email']} sudah terdaftar sebagai PANITIA_PENYELENGGARA")
                
                except:
                    messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
                    
                cursor.execute("set search_path to public")
    return render(request, "register_admin.html", response)

def login(request):
    # Check apakah sudah logged in, bila iya redirect ke landing
    if request.session.get("pengguna", False):
        return redirect("home")

    if request.method == "POST":
        is_found = False # Sudah ketemu?
        tipe_login = None # Warga / Panitia Penyelanggara / Admin Sistem

        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            
            # Check apakah Panitia Penyelanggara
            tipe_login = "Panitia Penyelanggara"
            cursor.execute(f"""SELECT * FROM PENGGUNA
                NATURAL JOIN PANITIA_PENYELENGGARA
                WHERE email = '{request.POST["email"]}'
                AND password = '{request.POST["password"]}'""")  

            row = cursor.fetchall()
            if (len(row) != 0):
                is_found = True
                
            if is_found:
                cursor.execute(f"""SELECT * FROM PENGGUNA
                NATURAL JOIN PANITIA_PENYELENGGARA
                NATURAL JOIN NAKES
                WHERE email = '{request.POST["email"]}'
                AND password = '{request.POST["password"]}'""") 
                nakes = cursor.fetchall()
                if len(nakes) != 0:
                    tipe_login = "Tenaga Kesehatan"
                    row = nakes
                
            # Check apakah Admin Satgas
            if not is_found:
                tipe_login = "Admin Satgas"
                cursor.execute(f"""SELECT * FROM PENGGUNA
                    NATURAL JOIN ADMIN_SATGAS
                    WHERE email = '{request.POST["email"]}'
                    AND password = '{request.POST["password"]}'""")  

                row = cursor.fetchall()
                if (len(row) != 0):
                    is_found = True
                
            # Check apakah Warga
            if not is_found:
                tipe_login = "Warga"
                cursor.execute(f"""SELECT * FROM PENGGUNA
                    NATURAL JOIN WARGA
                    WHERE email = '{request.POST["email"]}'
                    AND password = '{request.POST["password"]}'""")  

                row = cursor.fetchall()
                if (len(row) != 0):
                    is_found = True

            # Handler bila ditemukan
            if (len(row) != 0):
                cursor.execute(f"""UPDATE PENGGUNA 
                    SET status_verifikasi = 'sudah terverifikasi'
                    WHERE email = '{request.POST["email"]}'
                    AND password = '{request.POST["password"]}'""")
                cursor.execute("set search_path to public")
                request.session["tipe"] = tipe_login
                if tipe_login == "Warga":
                    request.session["pengguna"] = row[0][5]
                else:
                    request.session["pengguna"] = row[0][4]
                request.session["email"] = row[0][0] # Dapatkan email
                return redirect("home")
            
            # Handler bila tidak ditemukan penggunanya
            messages.add_message(request, messages.WARNING, f"Login gagal, check kembali password atau email atau coba lain kali")
            
            cursor.execute("set search_path to public")
    return render(request, 'login.html')

def logout(request):
    # Hilangkan semua dari session
    request.session.pop("pengguna", None)
    request.session.pop("tipe", None)

    return redirect("login")

def register_vaksinasi(request):
    response = {}
    response['instansi'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * from INSTANSI")
        instansi = cursor.fetchall()
        
        for i in range(len(instansi)):
            response['instansi'].append([
                instansi[i][0], instansi[i][1]
            ])
        cursor.execute("set search_path to public")
            
    if request.method == "POST":
        # Kalau valid semua, register
        with connection.cursor() as cursor:
            cursor.execute("set search_path to public")
            email = request.session["email"]
            cursor.execute("set search_path to sivax")
            try:
                                            
                cursor.execute(f"""
                    INSERT INTO WARGA (email, nik, nama_lengkap, jenis_kelamin, no, jalan, kelurahan, kecamatan, kabkot, instansi) VALUES
                    ('{email}', '{request.POST["nik"]}', '{request.POST["nama_lengkap"]}', '{request.POST["jenis_kelamin"]}',
                    '{request.POST["no"]}', '{request.POST["jalan"]}', '{request.POST["kelurahan"]}', '{request.POST["kecamatan"]}', 
                    '{request.POST["kabkot"]}', '{request.POST["instansi"]}')
                """)
                
                messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")

                return redirect("daftar")
            
            # Ada email yang sama, handle disini
            except InternalError:
                messages.add_message(request, messages.WARNING, f"{request.POST['email']} sudah terdaftar sebagai Warga")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
                    
        cursor.execute("set search_path to public")
    return render(request, "register_vaksinasi.html", response)
