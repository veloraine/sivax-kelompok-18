## Module users (Fitur Wajib)
**Dibuat oleh : Ramdhan - 2006595753**

Digunakan untuk login/registrasi/logout

Untuk mengecek apakah user sudah login maka bisa panggil perintah berikut,

```
request.session.get("pengguna", None)   # Return identifikasi user
request.session.get("tipe", None)       # Return "Warga", "Admin Satgas", atau "Panitia Penyelenggara"
```

Jika sudah logged in, akan dikembalikan identifikasi user. Jika belum logged in, akan dikembalikan value `None`

## URLs

| namespace          | URL 				   | 
| -----------------  | ------------------- | 
| login              | /login 			   |
| logout             | /logout 			   |
| register           | /logout 			   |
| register_warga     | /register_warga 	   |
| register_admin     | /register_admin	   |
| register_panitia   | /register_panitia   |
| register_vaksinasi | /register_vaksinasi |

'register_vaksinasi' digunakan untuk admin satgas ataupun panitia penyelenggara yang belum register warga saat ingin melakukan pendaftaran vaksinasi

## Asumsi
- Saat melakukan register sebagai warga, admin satgas ataupun panitia penyelenggara hanya register sebagai satu tipe pengguna
- Pengguna bertipe warga dapat melakukan registrasi sebagai salah satu dari admin satgas ataupun panitia penyelenggara, dengan syarat email, no_telp, dan password harus sama
- Panitia penyelenggara dapat dapat melakukan registrasi sebagai warga, dengan syarat email, no_telp, dan password harus sama
- Admin satgas dapat dapat melakukan registrasi sebagai warga, dengan syarat email, no_telp, dan password harus sama