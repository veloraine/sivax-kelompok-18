$(document).ready(function() {
    $('#example').DataTable();
} );

$("#tenaga_kesehatan").change(function () {
  var cekTenaga = document.querySelector("#form_tenaga_kesehatan");
  if ($(this).val() == "Ya") {
    cekTenaga.classList.remove('d-none');
    $("#no_str").attr("required", true);
    $("#tipe").attr("required", true);
    var str = document.querySelector("#no_str");
  } else if ($(this).val() == "Tidak"){
    cekTenaga.classList.add('d-none');
    $("#no_str").removeAttr("required");
    $("#tipe").removeAttr("required");
    var tipe = document.querySelector("#tipe");
  }
});

$("#kategori_faskes").change(function () {
  var cekFaskes = document.querySelector("#faskes");
  var cekNon = document.querySelector("#non")
  if ($(this).val() == "Faskes") {
    cekFaskes.classList.remove('d-none');
    cekNon.classList.add('d-none');
    $("#tipe_faskes").attr("required", true);
    $("#status_kepemilikan").attr("required", true);
    $("#kategori").removeAttr("required");
  }else if ($(this).val() == "Non-Faskes"){
    cekFaskes.classList.add('d-none');
    cekNon.classList.remove('d-none');
    $("#tipe_faskes").removeAttr("required");
    $("#status_kepemilikan").removeAttr("required");
    $("#kategori").attr("required", true);
  }
})

$("#jenis_akun").change(function () {
    var href = document.getElementById("form-action")
    if ($(this).val() == "warga") {
        href.action = "/register_warga"
    } else if ($(this).val() == "panitia"){
        href.action = "/register_panitia"
    } else if ($(this).val() == "admin"){
        href.action = "/register_admin"
    } else {
        href.action = "/register" 
    }
});

function deleteVaksin(kodeVaksin){
  url = `{% url 'delete_vaksin' ${kodeVaksin} %}` 
  $("#modal_delete").attr("href", url)
}

function nonFaskes(){
    var kategoriInstansi = document.getElementById("kategori_instansi")
    var kategoriInstansiB = document.getElementById("kategori_instansiB")
    kategoriInstansi.classList.add('d-none');
    kategoriInstansiB.classList.remove('d-none');

}
    function Faskes(){
    var kategoriInstansi = document.querySelector("#kategori_instansi")
    var kategoriInstansiB = document.getElementById("kategori_instansiB")
    kategoriInstansi.classList.remove('d-none');
    kategoriInstansiB.classList.add('d-none');
}

function deleteStatus() {
    swal({
        title: "HAPUS STATUS TIKET",
        text: "Anda yakin untuk menghapus 01 ?",
        icon: "warning",
        buttons: {
            cancel: "Tidak",
            catch: {
                text: "Ya",
                icon: "warning",
            },
        },
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          swal("Status berhasil dihapus", {
            icon: "success",
          });
        } else {
          swal("Status tidak jadi dihapus");
        }
      });
}

function deleteInstansi() {
    swal({
        title: "HAPUS INSTANSI",
        text: "Anda yakin untuk menghapus 01 ?",
        icon: "warning",
        buttons: {
            cancel: "Tidak",
            catch: {
                text: "Ya",
                icon: "warning",
            },
        },
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          swal("Instansi berhasil dihapus", {
            icon: "success",
          });
        } else {
          swal("Instansi tidak jadi dihapus");
        }
      });
}