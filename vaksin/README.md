## Module VAKSIN
**CD VAKSIN - Fitur 10 Dibuat oleh : Ramdhan - 2006595753**

**RU VAKSIN - Fitur 5 Dibuat oleh: Brandon Ivander - 2006535874**

## URLs

| namespace         | URL 				                                        | Fitur                 |
| ----------------- | --------------------------------------------------------- | --------------------- |
| update            | /update-vaksin/(?P<kode>[0-9]+)$/(?P<nama_vaksin>[0-9]+)$ | RU VAKSIN - FITUR 5   |
| tambah_udpdate    | /tambah-update/(?P<kode>[0-9]+)$/(?P<nama_vaksin>[0-9]+)$	| RU VAKSIN - FITUR 5   |
| create_vaksin     | /create_vaksin                                            | CD VAKSIN - Fitur 10  |
| delete_vaksin     | /delete_vaksin/(?P<vaksin>[^/]+)$                         | CD VAKSIN - Fitur 10  |