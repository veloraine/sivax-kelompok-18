from django.contrib.messages.api import add_message
from django.shortcuts import redirect, render
from .models import *
from django.contrib import messages
from django.db import connection

# Create your views here.

def vaksin(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * from VAKSIN")
        instansi = cursor.fetchall()
        response['daftar_vaksin'] = instansi
        cursor.execute("set search_path to public")
    return render(request, 'vaksin.html', response)

def create(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT kode FROM VAKSIN")
        data_kode = [item[0] for item in cursor.fetchall()]
        for i in range(1, 10000000000000000):
            kode_vaksin = "vcn0" + str(i)
            if kode_vaksin not in data_kode:
                break
    
        response['kode_vaksin'] = kode_vaksin
        cursor.execute("set search_path to public")

    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            try:
                cursor.execute(f"""
                    INSERT INTO VAKSIN (kode, nama, nama_produsen, no_edar, freq_suntik, stok) VALUES
                    ('{kode_vaksin}', '{request.POST["nama_vaksin"]}', '{request.POST["produsen"]}', 
                    '{request.POST["no_edar"]}', '{request.POST["stok"]}', '{request.POST["frekuensi"]}')
                """)
                return redirect("vaksin")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
            cursor.execute("set search_path to public")
    return render(request, 'create_vaksin.html', response)

def update(request, kode, nama_vaksin):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""
            SELECT email_pegawai, tgl_waktu, jumlah_update
            from UPDATE_STOK
            WHERE kode_vaksin = '{kode}';
            """)
        list_update = cursor.fetchall()

    context = {
        'kode' : kode,
        'nama_vaksin' : nama_vaksin,
        'list_update' : list_update,
    }
    return render(request, 'update.html', context)

def tambah_update(request, kode, nama_vaksin):
    context = {
        'kode' : kode,
        'nama_vaksin' : nama_vaksin,
    }
    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                INSERT INTO UPDATE_STOK VALUES (
                    'Mike.Ricciardelli@gmail.com', '{request.POST['tgl_waktu']}', {request.POST['jumlah_update']}, '{kode}'
                );
                """)
            cursor.execute(f"""
                UPDATE VAKSIN SET
                stok = stok + {request.POST['jumlah_update']}
                WHERE kode = '{kode}';
                """)
            return redirect('/vaksin')
    return render(request, 'tambah_update.html', context)

def delete_vaksin(request, vaksin):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        try:
            cursor.execute(f"""DELETE FROM VAKSIN WHERE kode = '{vaksin}'""")
        except:
            messages.add_message(request, messages.WARNING, f"Gagal menghapus Vaksin {vaksin}")
        cursor.execute("set search_path to public")
    return redirect("vaksin")

