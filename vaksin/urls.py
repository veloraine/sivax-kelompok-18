from django.urls import path
from . import views

urlpatterns = [
    path('', views.vaksin, name="vaksin"),
    path('update-vaksin/(?P<kode>[0-9]+)$/(?P<nama_vaksin>[0-9]+)$', views.update, name='update_vaksin'),
    path('tambah-update/(?P<kode>[0-9]+)$/(?P<nama_vaksin>[0-9]+)$', views.tambah_update, name='tambah_update'),
    path('create_vaksin', views.create, name='create_vaksin'),
    path('delete-vaksin/(?P<vaksin>[^/]+)$', views.delete_vaksin, name="delete_vaksin"),
]