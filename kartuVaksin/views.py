from django.db import connection
from django.shortcuts import redirect, render
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template

from xhtml2pdf import pisa
# Create your views here.

def kartu_vaksin(request):
    if not request.session.get("email", False):
        return redirect('login')
    email = request.session["email"]

    context = dict()
    with connection.cursor() as cursor:
        cursor.execute("SET search_path TO SIVAX")
        cursor.execute(f""" SELECT w.nama_lengkap, k.no_sertifikat, k.status_tahapan
                            FROM WARGA w, KARTU_VAKSIN k
                            WHERE k.email = '{email}' AND
                                  w.email = k.email;
                            """)
        context["cards"] = cursor.fetchall()
        cursor.execute("SET search_path TO PUBLIC")
    return render(request, "kartu_vaksin.html", context=context)


def render_to_pdf(template_src, context_dict={}):

    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None


def download_pdf(request, id):
    if not request.session.get("email", False):
        return redirect('login')
    
    context = dict()
    with connection.cursor() as cursor:
        cursor.execute("SET search_path TO SIVAX")
        cursor.execute(f""" SELECT w.nama_lengkap, k.no_sertifikat, k.status_tahapan
                        FROM WARGA w, KARTU_VAKSIN k
                        WHERE k.no_sertifikat = '{id}' AND
                                w.email = k.email;
                        """)
        context["srtf"] = cursor.fetchone()
        cursor.execute("SET search_path TO PUBLIC")
    
    pdf = render_to_pdf('pdf/kartu.html', context)
    response = HttpResponse(pdf, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=Kartu Vaksin.pdf' 
    return response