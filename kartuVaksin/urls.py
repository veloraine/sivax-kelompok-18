from django.urls import path
from . import views

urlpatterns = [
    path('', views.kartu_vaksin, name="kartu_vaksin"),
    path('get-kartu-vaksin/<id>', views.download_pdf, name="download_kartu")
]