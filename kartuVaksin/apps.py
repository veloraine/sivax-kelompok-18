from django.apps import AppConfig


class KartuvaksinConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kartuVaksin'
