# Module Distribusi (Fitur 4)

**Dibuat oleh Brandon Ivander - 2006535874**

## URLs

| namespace / use case  | URL 					 | 
| --------------------- | ---------------------- | 
| atur_distribusi       | /distribusi/atur-distribusi/(?P<nama_instansi>[0-9]+)$/(?P<tanggal_waktu>[0-9]+)$/(?P<jumlah>[0-9]+)$/(?P<status>[0-9]+)$    |
| daftar_distribusi | /distribusi/daftar-distribusi/ |
| detail_distribusi | /distribusi/detail-distribusi/(?P<kode>[0-9]+)$/(?P<tanggal>[0-9]+)$/(?P<biaya>[0-9]+)$/(?P<>[0-9]+)$/(?P<nama>[0-9]+)$/(?P<jumlah_vaksin>[0-9]+)$ |
| update_distribusi | /distribusi/update-distribusi/(?P<kode>[0-9]+)$ |
| hapus_distribusi | /distribusi/hapus-distribusi/(?P<kode>[0-9]+)$ |