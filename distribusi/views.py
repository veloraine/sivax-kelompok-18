from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
import datetime

# Create your views here.
def atur_distribusi(request, nama_instansi, tanggal_waktu, jumlah, status):
    email_verif = request.session['email']
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"SELECT kode FROM INSTANSI WHERE nama_instansi = '{nama_instansi}'")
        kode_instansi = cursor.fetchall()

        cursor.execute(f"""
            SELECT kategori_penerima, jumlah_nakes, email_admin, kode_lokasi 
            from PENJADWALAN
            WHERE kode_instansi = '{kode_instansi[0][0]}';
            """)
        data = cursor.fetchall()

        cursor.execute(f"""
            SELECT nama
            from LOKASI_VAKSIN
            WHERE kode = '{data[0][3]}'
            """)
        nama_lokasi = cursor.fetchall()

        # fetch select vaksin
        list_vaksin = []
        cursor.execute(f"""
            SELECT kode, nama
            from VAKSIN;
            """)
        list_vaksin = cursor.fetchall()
        # fetch kode dist
        cursor.execute(f"""
            select kode from distribusi
            order by kode desc
            limit 1;""")
        last_dist = cursor.fetchall()
        # inc kode dist
        new_kode = int(last_dist[0][0][3:])+1
        if len(str(new_kode)) == 1:
            new_kode = f"dst0{new_kode}"
        else:
            new_kode = f"dst{new_kode}"
            
        context = {
            'nama_instansi' : nama_instansi,
            'tanggal_waktu' : tanggal_waktu,
            'jumlah' : jumlah,
            'status' : status,
            'kategori_penerima' : data[0][0],
            'auto_kode' : new_kode,
            'jumlah_nakes' : data[0][1],
            'email_admin' : data[0][2],
            'nama_lokasi' : nama_lokasi[0][0],
            'list_vaksin' : list_vaksin
        }
        
        cursor.execute("set search_path to public")
    
    if request.method == "POST":
        today = datetime.datetime.now()
        nextday = today + datetime.timedelta(days=1)
        vaksinday = tanggal_waktu

        # setup string date
        str_today = str(today)[:10]
        str_nextday = str(nextday)[:10]
        str_vaksinday = str(vaksinday)[:10]

        if (str(request.POST['tanggal']) < str_nextday):
            messages.add_message(request, messages.WARNING, f"Waktu distribusi tidak boleh hari-H atau hari-H+1")
        elif (str(request.POST['tanggal']) >= str_vaksinday):
            messages.add_message(request, messages.WARNING, f"Waktu distribusi melebihi waktu pelaksanaan vaksin")
        else:
            try:
                with connection.cursor() as cursor:
                    cursor.execute("set search_path to sivax")
                    # modify current trigger for current logged in admin
                    cursor.execute(f"""
                        create or replace function update_vaksin() returns trigger
                            language plpgsql
                        as
                        $$
                        DECLARE
                            stok_tersedia integer;

                        BEGIN
                            -- mengambil stok tersedia
                            SELECT stok
                            INTO stok_tersedia
                            FROM vaksin
                            WHERE vaksin.kode = NEW.kode_vaksin;

                            -- kondisi jika stok lebih sedikit
                            IF stok_tersedia < NEW.jumlah_vaksin THEN
                                RAISE EXCEPTION 'JUMLAH VAKSIN TERSEDIA TIDAK MENCUKUPI';
                            ELSE
                                
                                INSERT INTO update_stok
                                VALUES ('{email_verif}', NEW.tanggal, -NEW.jumlah_vaksin, NEW.kode_vaksin);
                                -- mengurangi jumlah
                                UPDATE vaksin
                                SET stok = stok - NEW.jumlah_vaksin
                                WHERE vaksin.kode = NEW.kode_vaksin;
                            END IF;
                            RETURN NEW;
                        END;
                        $$;
                        """)
                    cursor.execute(f"""
                        INSERT INTO DISTRIBUSI VALUES (
                        '{new_kode}', 
                        '{request.POST['tanggal']}', 
                        '{request.POST['biaya']}', 
                        '{request.POST['jumlah_vaksin']}',
                        '{request.POST['vaksin']}'
                        );
                        """)
                    cursor.execute(f"""
                        UPDATE PENJADWALAN
                        SET kode_distribusi = '{new_kode}'
                        WHERE kode_instansi = '{kode_instansi[0][0]}';
                        """)
                    cursor.execute("set search_path to public")
                    return redirect('/penjadwalan/pengajuan')
            except:
                messages.add_message(request, messages.WARNING, f"Stok vaksin tidak mencukupi, harap coba lagi")

    return render(request, 'atur_distribusi.html', context)

def daftar_distribusi(request):
    response = {}
    response['distribusi'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute('''
            SELECT distribusi.kode, tanggal, biaya, nama, jumlah_vaksin
            FROM DISTRIBUSI, VAKSIN
            WHERE DISTRIBUSI.kode_vaksin = VAKSIN.kode;
            ''')
        distribusi = cursor.fetchall()

        for i in range(len(distribusi)):
            response['distribusi'].append(
                distribusi[i]
            )
        cursor.execute("set search_path to public")
    return render(request, 'daftar_distribusi.html', response)

def detail_distribusi(request, kode, tanggal, biaya, nama, jumlah_vaksin):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"SELECT kode_instansi FROM PENJADWALAN WHERE kode_distribusi = '{kode}'")
        kode_instansi = cursor.fetchall()

        cursor.execute(f"""
            SELECT kategori_penerima, jumlah_nakes, email_admin, kode_lokasi, status, jumlah_nakes, tanggal_waktu
            from PENJADWALAN
            WHERE kode_instansi = '{kode_instansi[0][0]}';
            """)
        data = cursor.fetchall()

        cursor.execute(f"""
            SELECT nama
            from LOKASI_VAKSIN
            WHERE kode = '{data[0][3]}'
            """)
        nama_lokasi = cursor.fetchall()

        cursor.execute(f"""
            select nama_instansi
            from INSTANSI
            WHERE kode = '{kode_instansi[0][0]}';
            """)
        nama_instansi = cursor.fetchall()

    context = {
        'nama_instansi' : nama_instansi[0][0],
        'tanggal_waktu' : data[0][5],
        'jumlah' : data[0][4],
        'status' : data[0][3],
        'lokasi_vaksin' : nama_lokasi[0][0],
        'kategori_penerima' : data[0][0],
        'jumlah_nakes' : data[0][1],
        'email_admin' : data[0][2],
        'kode' : kode,
        'tanggal' : tanggal,
        'biaya' : biaya,
        'nama' : nama,
        'jumlah_vaksin' : jumlah_vaksin
    }
    return render(request, 'detail_distribusi.html', context)

def update_distribusi(request, kode):
    email_verif = request.session['email']
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"SELECT kode_instansi FROM PENJADWALAN WHERE kode_distribusi = '{kode}'")
        kode_instansi = cursor.fetchall()

        cursor.execute(f"""
            SELECT kategori_penerima, jumlah_nakes, email_admin, kode_lokasi, status, jumlah_nakes, tanggal_waktu
            from PENJADWALAN
            WHERE kode_instansi = '{kode_instansi[0][0]}';
            """)
        data = cursor.fetchall()

        cursor.execute(f"""
            SELECT nama
            from LOKASI_VAKSIN
            WHERE kode = '{data[0][3]}'
            """)
        nama_lokasi = cursor.fetchall()

        cursor.execute(f"""
            select nama_instansi
            from INSTANSI
            WHERE kode = '{kode_instansi[0][0]}';
            """)
        nama_instansi = cursor.fetchall()
        
        # fetch select vaksin
        list_vaksin = []
        cursor.execute(f"""
            SELECT kode, nama
            from VAKSIN;
            """)
        list_vaksin = cursor.fetchall()

        context = {
            'nama_instansi' : nama_instansi[0][0],
            'tanggal_waktu' : data[0][5],
            'jumlah' : data[0][4],
            'status' : data[0][3],
            'lokasi_vaksin' : nama_lokasi[0][0],
            'kategori_penerima' : data[0][0],
            'jumlah_nakes' : data[0][1],
            'email_admin' : data[0][2],
            'kode' : kode,
            'list_vaksin' : list_vaksin
        }
        
        cursor.execute("set search_path to public")
    
    if request.method == "POST":
        today = datetime.datetime.now()
        nextday = today + datetime.timedelta(days=1)
        vaksinday = data[0][5]

        # setup string date
        str_today = str(today)[:10]
        str_nextday = str(nextday)[:10]
        str_vaksinday = str(vaksinday)[:10]

        if (str(request.POST['tanggal']) < str_nextday):
            print("masuk sini gan")
        elif (str(request.POST['tanggal']) >= str_vaksinday):
            print("UDAH LEWAT")
        else:
            with connection.cursor() as cursor:
                cursor.execute("set search_path to sivax")
                try:
                    # modify current trigger for current logged in admin
                    cursor.execute(f"""
                        create or replace function update_vaksin() returns trigger
                            language plpgsql
                        as
                        $$
                        DECLARE
                            stok_tersedia integer;

                        BEGIN
                            -- mengambil stok tersedia
                            SELECT stok
                            INTO stok_tersedia
                            FROM vaksin
                            WHERE vaksin.kode = NEW.kode_vaksin;

                            -- kondisi jika stok lebih sedikit
                            IF stok_tersedia < NEW.jumlah_vaksin THEN
                                RAISE EXCEPTION 'JUMLAH VAKSIN TERSEDIA TIDAK MENCUKUPI';
                            ELSE
                                
                                INSERT INTO update_stok
                                VALUES ('{email_verif}', NEW.tanggal, -NEW.jumlah_vaksin, NEW.kode_vaksin);
                                -- mengurangi jumlah
                                UPDATE vaksin
                                SET stok = stok - NEW.jumlah_vaksin
                                WHERE vaksin.kode = NEW.kode_vaksin;
                            END IF;
                            RETURN NEW;
                        END;
                        $$;
                        """)
                    cursor.execute(f"""
                        UPDATE DISTRIBUSI SET 
                        tanggal = '{request.POST['tanggal']}', 
                        biaya = '{request.POST['biaya']}', 
                        jumlah_vaksin = '{request.POST['jumlah_vaksin']}',
                        kode_vaksin = '{request.POST['vaksin']}'
                        WHERE kode = '{kode}';
                        """)
                    cursor.execute("set search_path to public")
                    return redirect('/distribusi/daftar-distribusi')
                except:
                    messages.add_message(request, messages.WARNING, f"Stok vaksin tidak mencukupi, harap coba lagi")

    return render(request, 'update_distribusi.html', context)

def hapus_distribusi(request, kode):
    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                UPDATE PENJADWALAN SET
                kode_distribusi = null
                WHERE kode_distribusi = '{kode}';
                """)
            cursor.execute(f"""
                DELETE FROM DISTRIBUSI
                WHERE kode = '{kode}';
                """)
            
            cursor.execute("set search_path to public")
            return redirect("/distribusi/daftar-distribusi/")
    return render(request, 'hapus_distribusi.html')