from django.urls import path
from . import views

urlpatterns = [
    path('atur-distribusi/(?P<nama_instansi>[0-9]+)$/(?P<tanggal_waktu>[0-9]+)$/(?P<jumlah>[0-9]+)$/(?P<status>[0-9]+)$', views.atur_distribusi, name="atur_distribusi"),
    path('daftar-distribusi/', views.daftar_distribusi),   
    path('detail-distribusi/(?P<kode>[0-9]+)$/(?P<tanggal>[0-9]+)$/(?P<biaya>[0-9]+)$/(?P<>[0-9]+)$/(?P<nama>[0-9]+)$/(?P<jumlah_vaksin>[0-9]+)$', views.detail_distribusi, name='detail_distribusi'),
    path('update-distribusi/(?P<kode>[0-9]+)$', views.update_distribusi, name="update_distribusi"),
    path('hapus-distribusi/(?P<kode>[0-9]+)$', views.hapus_distribusi, name='hapus_distribusi')
]