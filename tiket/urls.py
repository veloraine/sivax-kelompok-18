from django.urls import path
from . import views

urlpatterns = [
    path('', views.tiket_saya, name="tiket_saya"),
    path('detail/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$/(?P<nomor>[0-9]+)$/(?P<status>[0-9]+)$', views.detail_tiket, name="detail_kartu"),
    path('jadwal_vaksinasi', views.jadwal, name="jadwal"),
    path('daftar_vaksinasi/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$', views.daftar, name="daftar"),
    path('daftar_vaksinasi/mendaftar/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$', views.mendaftar, name="mendaftar"),
]