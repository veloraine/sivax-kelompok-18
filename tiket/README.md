## Module CR Tiket
**CR Tiket - Fitur 10 Dibuat oleh : Ramdhan - 2006595753**

## URLs

| namespace     | URL 				                                                                                | Fitur                 |
| ------------- | ------------------------------------------------------------------------------------------------- | --------------------- |
| tiket_saya    | /tiket                                                                                            | CR Tiket - Fitur 9    |
| detail_kartu  | /tiket/detail/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$/(?P<nomor>[0-9]+)$/(?P<status>[0-9]+)$    | CR Tiket - Fitur 9    |
| jadwal        | /tiket/jadwal_vaksinasi                                                                           | CR Tiket - Fitur 9    |
| daftar        | /tiket/daftar_vaksinasi/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$                                 | CR Tiket - Fitur 9    |
| mendaftar     | /tiket/daftar_vaksinasi/mendaftar/(?P<instansi>[0-9]+)$/(?P<jadwal>[0-9]+)$                       | CR Tiket - Fitur 9    |