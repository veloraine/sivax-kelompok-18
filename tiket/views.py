from django.shortcuts import redirect, render
from .models import *
from django.contrib import messages
from django.db import connection

# Create your views here.

def tiket_saya(request):
    response = {}
    response['kumpulan_tiket'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to public")
        email = request.session["email"]
        
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""SELECT * from TIKET WHERE email = '{email}'""")
        kumpulan_tiket = cursor.fetchall()
        
        for i in range(len(kumpulan_tiket)):
            cursor.execute("SELECT * FROM INSTANSI WHERE kode = %s", [kumpulan_tiket[i][2]])
            instansi = cursor.fetchall()
            cursor.execute("SELECT * FROM STATUS_TIKET WHERE kode = %s", [kumpulan_tiket[i][3]])
            status_tiket = cursor.fetchall()
            response['kumpulan_tiket'].append([
                kumpulan_tiket[i][0], kumpulan_tiket[i][1], instansi[0][1], status_tiket[0][1], kumpulan_tiket[i][4]
            ])
        cursor.execute("set search_path to public")
    return render(request, 'tiket_saya.html', response)

def detail_tiket(request, instansi, jadwal, nomor, status):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""SELECT * FROM INSTANSI WHERE nama_instansi = '{instansi}'""")
        kode_nama = cursor.fetchall()
        
        cursor.execute(f"""SELECT * FROM PENJADWALAN WHERE kode_instansi = '{kode_nama[0][0]}' AND tanggal_waktu = '{jadwal}'""")
        penjadwalan = cursor.fetchall()
        
        cursor.execute(f"""SELECT * FROM LOKASI_VAKSIN WHERE kode = '{penjadwalan[0][6]}'""")
        lokasi_vaksin = cursor.fetchall()
        
        context = {
            'nama_instansi' : instansi,
            'jadwal' : jadwal,
            'kuota' : penjadwalan[0][2],
            'kategori' : penjadwalan[0][3],
            'nama_lokasi' : lokasi_vaksin[0][1],
            'nomor_tiket' : nomor,
            'status_tiket' : status,
        }

        cursor.execute("set search_path to public")
    return render(request, 'detail_tiket.html', context)

def jadwal(request):
    response = {}
    response['jadwal_vaksin'] = []
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("SELECT * FROM PENJADWALAN WHERE status = 'pengajuan disetujui'")
        jadwal = cursor.fetchall()
        
        for i in range(len(jadwal)):
            cursor.execute("SELECT * FROM INSTANSI WHERE kode = %s", [jadwal[i][0]])
            instansi = cursor.fetchall()
            response['jadwal_vaksin'].append([
                instansi[0][1], jadwal[i][1], jadwal[i][2]
            ])
        cursor.execute("set search_path to public")        
    return render(request, 'jadwal.html', response)

def daftar(request, instansi, jadwal):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""SELECT * FROM INSTANSI WHERE nama_instansi = '{instansi}'""")
        kode_nama = cursor.fetchall()
        
        cursor.execute(f"""SELECT * FROM PENJADWALAN WHERE kode_instansi = '{kode_nama[0][0]}' AND tanggal_waktu = '{jadwal}'""")
        penjadwalan = cursor.fetchall()
        
        cursor.execute(f"""SELECT * FROM LOKASI_VAKSIN WHERE kode = '{penjadwalan[0][6]}'""")
        lokasi_vaksin = cursor.fetchall()
        
        context = {
            'nama_instansi' : instansi,
            'jadwal' : jadwal,
            'kuota' : penjadwalan[0][2],
            'kategori' : penjadwalan[0][3],
            'nama_lokasi' : lokasi_vaksin[0][1],
        }
        cursor.execute("set search_path to public")        
    return render(request, 'daftar.html', context)

def mendaftar(request, instansi, jadwal):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to public")
        email = request.session["email"]
        
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""SELECT * FROM WARGA WHERE email = '{email}'""")
        isWarga = cursor.fetchall()
        
        if len(isWarga) == 0:
            messages.add_message(request, messages.WARNING, f"Registrasi terlebih dahulu sebagai Warga")
            return redirect("daftar", instansi, jadwal)
        else:
            cursor.execute(f"""SELECT * FROM INSTANSI WHERE nama_instansi = '{instansi}'""")
            kode_instansi = cursor.fetchall()[0][0]
            
            cursor.execute("SELECT COUNT(*) FROM TIKET")
            banyak_tiket = cursor.fetchall()[0][0]
            urutan_tiket = 'tkt0' + str(banyak_tiket + 1)
            
            try:
                cursor.execute(f"""INSERT INTO TIKET(email, no_tiket, kode_instansi, tgl_waktu) 
                        VALUES ('{email}', '{urutan_tiket}', '{kode_instansi}', '{jadwal}')""")
            except:
                messages.add_message(request, messages.WARNING, f"Anda sudah vaksin dosis ke-2 atau dalam 14 hari kebelakang anda sudah melakukan vaksin")
                return redirect("daftar", instansi, jadwal)
        
        cursor.execute("set search_path to public")
    return redirect("jadwal")