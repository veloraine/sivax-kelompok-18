from django.db import connection
from django.http import response
from django.shortcuts import redirect, render
from .models import *   

# Create your views here.

def daftarInstansi(request):
    response = {}
    response['instansi'] = []

    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute("""
            SELECT kode, nama_instansi
            FROM INSTANSI
            """)
        instansi = cursor.fetchall()

        for i in range(len(instansi)):
            response['instansi'].append(
                instansi[i]
            )
        cursor.execute("set search_path to public")       
    return render(request, 'daftar_instansi.html', response)

def editInstansi(request, kode):
    context = {
        'kode' : kode
    }
    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                SELECT *
                FROM INSTANSI_FASKES
                WHERE kode_instansi = '{kode}';
                """)
            faskes = cursor.fetchall()

            cursor.execute(f"""
                SELECT *
                FROM INSTANSI_NON_FASKES
                WHERE kode_instansi = '{kode}';
                """)
            nonFaskes = cursor.fetchall()

            cursor.execute(f"""
                UPDATE INSTANSI SET
                nama_instansi = '{request.POST['nama_instansi']}'
                WHERE kode = '{kode}';
                """)
            cursor.execute(f"""
                UPDATE INSTANSI_TELEPON SET
                no_telp = '{request.POST['no_telp']}'
                WHERE kode_instansi = '{kode}';
                """)
            if len(faskes) == 1 and len(nonFaskes) == 0:
                if request.POST['kategori_faskes'] == "FASKES":
                    cursor.execute(f"""
                        UPDATE INSTANSI_FASKES SET
                        tipe = '{request.POST['tipe_faskes']}',
                        status_kepemilikan = '{request.POST['status_kepemilikan']}'
                        WHERE kode_instansi = '{kode}';
                        """)
                elif request.POST['kategori_faskes'] == "NON":
                    cursor.execute(f"""
                        INSERT INTO INSTANSI_NON_FASKES VALUES (
                            '{kode}',
                            '{request.POST['kategori']}'
                        );
                        """)
                    cursor.execute(f"""
                        DELETE FROM INSTANSI_FASKES
                        WHERE kode_instansi = '{kode}';
                        """)
            elif len(faskes) == 0 and len(nonFaskes) == 1:
                if request.POST['kategori_faskes'] == "FASKES":
                    cursor.execute(f"""
                        INSERT INTO INSTANSI_FASKES VALUES (
                            '{kode}',
                            '{request.POST['tipe_faskes']}',
                            '{request.POST['status_kepemilikan']}'
                        );
                        """)
                    cursor.execute(f"""
                        DELETE FROM INSTANSI_NON_FASKES
                        WHERE kode_instansi = '{kode}';
                        """)
                elif request.POST['kategori_faskes'] == "NON":
                    cursor.execute(f"""
                        UPDATE INSTANSI_NON_FASKES SET
                        kategori = '{request.POST['kategori']}'
                        WHERE kode_instansi = '{kode}';
                        """)
            cursor.execute("set search_path to public")
            return redirect("/instansi")

    return render(request, 'update_instansi.html', context)

def detailInstansi(request, kode):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""
            SELECT nama_instansi
            FROM INSTANSI
            WHERE kode = '{kode}';
            """)
        nama_instansi = cursor.fetchall()

        cursor.execute(f"""
            SELECT no_telp
            FROM INSTANSI_TELEPON
            WHERE kode_instansi = '{kode}';
            """)
        no_telp = cursor.fetchall()

        cursor.execute(f"""
            SELECT tipe, statuskepemilikan
            FROM INSTANSI_FASKES
            WHERE kode_instansi = '{kode}';
            """)
        faskes = cursor.fetchall()
        if len(faskes) == 0:
            faskes.append([])
            faskes[0].append("a")
            faskes[0].append("b")

        cursor.execute(f"""
            SELECT kategori
            FROM INSTANSI_NON_FASKES
            WHERE kode_instansi = '{kode}';
            """)
        nonFaskes = cursor.fetchall()
        if len(nonFaskes) == 0:
            nonFaskes.append([])
            nonFaskes[0].append("a")

    if len(faskes) == 1:
        kategori_instansi = "Faskes"
    elif len(nonFaskes) == 1:
        kategori_instansi = "Non-Faskes"

    context = {
        'kode' : kode,
        'nama_instansi' : nama_instansi[0][0],
        'not_telp' : no_telp[0][0],
        'kategori_instansi' : kategori_instansi,
        'tipe_faskes' : faskes[0][0],
        'status_kepemilikan' : faskes[0][1],
        'kategori' : nonFaskes[0][0]
    }

    return render(request, 'detail_instansi.html', context)

def createInstansi(request):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""
            SELECT kode FROM INSTANSI
            ORDER BY kode desc
            limit 1;
            """)
        last_dist = cursor.fetchall()
        new_kode = int(last_dist[0][0][1:])+1
        if len(str(new_kode)) == 1:
            new_kode = f"D00{new_kode}"
        elif len(str(new_kode)) == 2:
            new_kode = f"D0{new_kode}"
        else:
            new_kode = f"D{new_kode}"

        context = {
            'auto_kode' : new_kode
        }

        cursor.execute("set search_path to public")
    
    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            cursor.execute(f"""
                INSERT INTO INSTANSI VALUES (
                    '{new_kode}',
                    '{request.POST['nama_instansi']}'
                );
                """)
            cursor.execute(f"""
                INSERT INTO INSTANSI_TELEPON VALUES (
                    '{new_kode}',
                    '{request.POST['not_telp_baru']}'
                );
                """)
            if request.POST["kategori_faskes"] == "FASKES":
                cursor.execute(f"""
                    INSERT INTO INSTANSI_FASKES VALUES (
                        '{new_kode}',
                        '{request.POST['tipe_faskes']}',
                        '{request.POST['status_kepemilikan']}'
                    );
                    """)
            elif request.POST["kategori_faskes"] == "NON":
                cursor.execute(f"""
                    INSERT INTO INSTANSI_NON_FASKES VALUES (
                        '{new_kode}'
                        '{request.POST['kategori']}'
                    );
                    """)
            cursor.execute("set search_path to public")
            return redirect('/instansi')

    return render(request, 'create_instansi.html', context)

def deleteInstansi(request, kode):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to sivax")
        cursor.execute(f"""
            SELECT kode_instansi, tipe, statuskepemilikan
            FROM INSTANSI_FASKES
            WHERE kode_instansi = '{kode}';
            """)
        hasil = cursor.fetchall()
        if len(hasil) == 0:
            cursor.execute(f"""
                SELECT kode_instansi, kategori
                FROM INSTANSI_NON_FASKES
                WHERE kode_instansi = '{kode}';
                """)
            hasil = cursor.fetchall()
        cursor.execute("set search_path to public")

            
    if request.method == "POST":
        with connection.cursor() as cursor:
            cursor.execute("set search_path to sivax")
            if len(hasil) == 3:
                cursor.execute(f"""
                    DELETE FROM INSTANSI_FASKES
                    WHERE kode_instansi = '{kode}';
                    """)
            else:
                cursor.execute(f"""
                    DELETE FROM INSTANSI_NON_FASKES
                    WHERE kode_instansi = '{kode}';
                    """)
            cursor.execute(f"""
                DELETE FROM INSTANSI_TELEPON
                WHERE kode_instansi = '{kode}';
                """)
            cursor.execute(f"""
                DELETE FROM INSTANSI
                WHERE kode = '{kode}';
                """)
            cursor.execute("set search_path to public")
            return redirect("/instansi")
    return render(request, "delete_instansi.html")