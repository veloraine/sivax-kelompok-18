from django.urls import path
from . import views

urlpatterns = [
    path('', views.daftarInstansi),
    path('update/(?P<kode>[0-9]+)$', views.editInstansi, name="update_instansi"),
    path('detail/(?P<kode>[0-9]+)$', views.detailInstansi, name="detail_instansi"),
    path('create/', views.createInstansi, name="create_instansi"),
    path('delete/(?P<kode>[0-9]+)$', views.deleteInstansi, name="delete_instansi"),
]