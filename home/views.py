from django.shortcuts import render
from .models import *
from django.db import connection

# Create your views here.
def home(request):
    return render(request, 'home.html')

def home_panitia(request):
    return render(request, 'home_panitia.html')

def home_admin(request):
    return render(request, 'home_admin.html')
