from django.urls import path
from . import views

urlpatterns = [
    path('home/', views.home, name='home'),
    path('home-panitia/', views.home_panitia, name='home_panitia'),
    path('home-admin/', views.home_admin, name='home_admin'),
]
