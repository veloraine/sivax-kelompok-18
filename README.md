# SIVAX - Tugas Kelompok 18 Basis Data Reguler Kelas (B)

Repository ini menggunakan template deploy ke heroku milik Kak Sage yang dapat ditemukan di [sini](https://github.com/laymonage/django-template-heroku)

## Link Website

https://b18-sivax.herokuapp.com/

## Anggota Kelompok

1. Brandon Ivander - 2006535874
2. Ferdinand Amos Papilaya - 2006536486
3. Ramdhan Firdaus Amelia - 2006595753
4. Rizki Kurniawan - 2006595785

## Instalasi

Untuk install ke local silahkan clone repository ini
```
git clone https://gitlab.com/Veloraine/sivax-kelompok-18.git
```
buat virtual environment (dengan env) dan jalankan
```
pip install -r requirements.txt
```

## Sample akun yang terdaftar pada sistem
- Panitia penyelenggara Non Nakes: 
nonNakes@gmail.com 
NON12345

- Panitia penyelenggara Non Nakes + Warga:
wargaNonNakes@gmail.com
WNN12345

- Panitia penyelenggara Nakes:
nakes@gmail.com
NAKES12345

- Panitia penyelenggara Nakes + Warga:
wargaNakes@gmail.com
WN12345

- Warga:
warga@gmail.com
WARGA12345

- Admin Satgas:
admin@gmail.com
ADMIN12345

- Admin Satgas + Warga:
wargaAdmin@gmail.com
WA12345


