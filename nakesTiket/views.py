from django.db import connection
from django.shortcuts import redirect, render
from django.http.response import HttpResponseNotFound, JsonResponse
import json
# Create your views here.

def list_tiket(request):
    if request.session.get("tipe", "") != "Tenaga Kesehatan":
        return redirect("login")

    context = {"tiket_list" : tuple()}
    with connection.cursor() as cursor:
        cursor.execute("SET search_path TO SIVAX")
        cursor.execute("""  SELECT t.no_tiket, w.nama_lengkap,  t.tgl_waktu, INITCAP(st.nama_status), i.nama_instansi 
                            FROM WARGA w, tiket t, STATUS_TIKET st, INSTANSI i
                            WHERE w.email = t.email AND
                                  st.kode = t.kode_status AND
                                  t.kode_instansi = i.kode;
                       """)
        rows = sorted(cursor.fetchall(), key=lambda x: int(x[0][3:]))
        context["tiket_list"] = rows
        cursor.execute("SET search_path TO PUBLIC")
    return render(request, 'r_tiket_nakes.html', context=context)


def detail_tiket(request, no_tiket):
    if request.session.get("tipe", "") != "Tenaga Kesehatan":
        return redirect("login")
    context = dict()
    with connection.cursor() as cursor:
        cursor.execute("SET search_path TO SIVAX")
        cursor.execute(f"""  SELECT t.no_tiket, w.nama_lengkap, w.email, t.tgl_waktu, INITCAP(st.nama_status), i.nama_instansi 
                            FROM WARGA w, tiket t, STATUS_TIKET st, INSTANSI i
                            WHERE t.no_tiket = '{no_tiket}' AND
                                  w.email = t.email AND
                                  st.kode = t.kode_status AND
                                  t.kode_instansi = i.kode;
                       """)
        data = cursor.fetchone()
        print(data)
        context = {
            "no_tiket": data[0],
            "nama_pengguna": data[1],
            "email_pengguna":data[2],
            "jadwal": data[3],
            "status_tiket": data[4],
            "nama_instansi": data[5]
        }
        cursor.execute("SET search_path TO PUBLIC")
    
    return render(request, "detail_tiket_nakes.html", context=context)

def ubah_status_tiket(request, id):
    if request.session.get("tipe", "") != "Tenaga Kesehatan":
        return redirect("login")

    context = {"tiket":tuple()}

    with connection.cursor() as cursor:
        cursor.execute("SET search_path TO SIVAX")
        cursor.execute(f""" SELECT  i.nama_instansi, 
                                    p.tanggal_waktu, 
                                    p.jumlah, 
                                    INITCAP(p.kategori_penerima), 
                                    l.nama, 
                                    t.no_tiket, 
                                    INITCAP(st.nama_status) 
                            FROM PENJADWALAN p, TIKET t, LOKASI_VAKSIN l, INSTANSI i, STATUS_TIKET st
                            WHERE   t.no_tiket = '{id}' AND
                                    t.kode_instansi = p.kode_instansi AND
                                    t.tgl_waktu = p.tanggal_waktu AND
                                    p.kode_instansi = i.kode AND
                                    p.kode_lokasi = l.kode AND
                                    t.kode_status = st.kode;
                        """)
        context["tiket"] = cursor.fetchone()
        cursor.execute("SET search_path TO PUBLIC")
    
    if context["tiket"][6] == "Terdaftar":
       context["options"] = ["Siap Vaksin", "Tidak Lolos Screening"]
    elif context["tiket"][6] == "Siap Vaksin":
        context["options"] = ["Selesai Vaksin"]
    context["status_awal"] = context["tiket"][6]
    return render(request, 'u_tiket_nakes.html', context=context)


def update(request):
    if request.session.get("tipe", "") != "Tenaga Kesehatan":
        return redirect("login")
    
    if request.POST:
        data = json.loads(request.POST["content"])

        if data["is_change"]:
            with connection.cursor() as cursor:
                cursor.execute("SET search_path TO SIVAX")
                new_status = data["new_status"].lower()
                cursor.execute(f""" UPDATE tiket
                                    SET kode_status = (SELECT kode FROM STATUS_TIKET WHERE nama_status = '{new_status}')
                                    WHERE no_tiket = '{data["no_tiket"]}';
                                """)
                
                cursor.execute("SET search_path TO PUBLIC")

        return JsonResponse("", safe=False)
    return HttpResponseNotFound()