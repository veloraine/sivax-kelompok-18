from django.urls import path
from . import views

urlpatterns = [
    path('', views.list_tiket, name="tiket_nakes"),
    path('ubah-status-tiket/<id>', views.ubah_status_tiket, name="ubah_tiket_nakes"),
    path('ajax/update', views.update),
    path('detail-tiket-nakes/<no_tiket>', views.detail_tiket, name="detail_tiket_nakes"),
]